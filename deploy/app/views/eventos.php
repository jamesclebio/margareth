<div class="modal-header">
	<h6 class="color-2">Novidades</h6>
</div>

<div class="modal-body">
	<div class="news-viewer">
		<section class="text">
			<?php include 'eventos/' . $this->_get('k') . '.php'; ?>
		</section>

		<?php if ($this->_get('v')): ?>
			<div class="separate">
				<div class="block-embed">
					<iframe src="http://www.youtube.com/embed/<?php echo $this->_get('v'); ?>" frameborder="0" width="560" height="315"></iframe>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this->_get('g')): ?>
			<div class="separate">
				<ul class="list-thumb">
					<?php $this->galeria(); ?>
				</ul>
			</div>
		<?php endif; ?>
	</div>
</div>

<div class="modal-footer">
	<a href="#" class="button button-2" data-modal-close>Fechar</a>
</div>
