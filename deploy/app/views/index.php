<!-- Início (billboard) -->
<section class="section-page" id="inicio">
	<div class="billboard">
		<div class="billboard-container">
			<div class="item item-center">
				<img src="<?php echo $this->_asset('default/images/billboard/01.jpg'); ?>" alt="" class="image">
			</div>
			<div class="item item-center">
				<img src="<?php echo $this->_asset('default/images/billboard/02.jpg'); ?>" alt="" class="image">
			</div>
			<div class="item item-center">
				<img src="<?php echo $this->_asset('default/images/billboard/03.jpg'); ?>" alt="" class="image">
			</div>
			<div class="item item-center">
				<img src="<?php echo $this->_asset('default/images/billboard/04.jpg'); ?>" alt="" class="image">
			</div>
			<div class="item item-center">
				<img src="<?php echo $this->_asset('default/images/billboard/05.jpg'); ?>" alt="" class="image">
			</div>
		</div>
	</div>
</section>

<!-- O ateliê -->
<div class="section-page" id="atelie">
	<div class="section-page-container">
		<header class="section-page-header">
			<h1>O ateliê</h1>
			<h3>Saiba um pouco mais sobre nós</h3>
		</header>

		<div class="section-page-content">
			<div class="align-center">
				<div class="block-avatar">
					<img src="<?php echo $this->_asset('default/images/margareth.jpg'); ?>" alt="">
				</div>
			</div>

			<div class="margin-top-25 text">
				<p>O Ateliê Margareth Souvenirs, nasceu para atender familiares e amigos, hoje é uma empresa com 30 anos de experiência, voltada para o mercado de lembranças  para nascimento, batizado, eucaristia, debutantes, bodas, casamento e corporativos, utilizando as mais modernas e exclusivas linha de produtos inovadores, destacando-se pela sua qualidade e atendimento individualizado.</p>
				<p>Nesses 30 anos de trabalho, sentimos a necessidade de oferecer mais conforto para nossos clientes, e no dia 15 de abril de 2014, inauguramos nossa sede própria, dispondo de pronta entrega e criações exclusivas.</p>
				<p>As criações são exclusivas para cada encomenda, pois não possuímos um catálogo fixo de imagens (os itens são demonstrativos e expostos de acordo com trabalho realizado). Após consultar nossos produtos, o cliente entra em contato conosco dando maiores referências sobre o tema, cores e detalhes desejados.</p>
				<p>Nosso objetivo é fazer parte do seu momento de uma forma especial e única com criatividade e inovação criando assim memórias inesquecíveis. Grande parte dos nossos produtos são produzidos de forma artesanal caracterizado pelo fino e enriquecido acabamento que fazem a diferença no olhar, no tato e até mesmo na audição ao ouvir os grandes e felizes elogios de quem recebe um detalhe Ateliê Margareth Souvenirs.</p>
				<p>As cores e materiais utilizados são escolhidos em comum acordo, combinado com o projeto aprovado e seguindo o estilo de trabalho que realizamos. Os produtos são de alta qualidade, garantindo maior durabilidade e resistência.</p>
				<p>As lembranças já incluem embalagem e cartão de agradecimento, conforme combinado.</p>
			</div>
		</div>
	</div>
</div>

<!-- Nossos produtos -->
<div class="section-page section-page-a" id="produtos">
	<div class="section-page-container">
		<header class="section-page-header">
			<h1>Nossos produtos</h1>
		</header>

		<div class="section-page-content">
			<div class="showcase">
				<div class="showcase-index">
					<ul>
						<li><a href="<?php echo $this->_url('produtos/nascimento'); ?>">Nascimento</a></li>
						<li><a href="<?php echo $this->_url('produtos/batizado'); ?>">Batizado</a></li>
						<li><a href="<?php echo $this->_url('produtos/eucaristia'); ?>">Eucaristia</a></li>
						<li><a href="<?php echo $this->_url('produtos/quinzeanos'); ?>">15 anos</a></li>
						<li class="on"><a href="<?php echo $this->_url('produtos/casamento'); ?>">Casamento</a></li>
						<li><a href="<?php echo $this->_url('produtos/formatura'); ?>">Formatura</a></li>
						<li><a href="<?php echo $this->_url('produtos/corporativo'); ?>">Corporativo</a></li>
						<li><a href="<?php echo $this->_url('produtos/bodas'); ?>">Bodas</a></li>
						<li><a href="<?php echo $this->_url('produtos/outros'); ?>">Outros</a></li>
					</ul>
				</div>
				<div class="showcase-grid"></div>
			</div>
		</div>
	</div>
</div>

<!-- Eventos -->
<div class="section-page" id="eventos">
	<div class="section-page-container">
		<header class="section-page-header">
			<h1>Eventos</h1>
		</header>

		<div class="section-page-content">
			<div class="news">
				<ul class="news-items">
					<li>
						<a href="<?php echo $this->_url('eventos/index/k/10/g/workshop-casar-se-2014'); ?>" data-modal>
							<img src="<?php echo $this->_asset('default/images/eventos/index/10.jpg'); ?>" alt="">
							<span class="title">Workshop Casar-SE 2014</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->_url('eventos/index/k/9/g/inauguracao-atelie'); ?>" data-modal>
							<img src="<?php echo $this->_asset('default/images/eventos/index/9.jpg'); ?>" alt="">
							<span class="title">Inauguração do Ateliê Margareth Souvenirs</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->_url('eventos/index/k/8/g/workshop-casar-se-2013'); ?>" data-modal>
							<img src="<?php echo $this->_asset('default/images/eventos/index/8.jpg'); ?>" alt="">
							<span class="title">Workshop Casar-SE 2013</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->_url('eventos/index/k/7/g/1-mostra-noivas-aracaju'); ?>" data-modal>
							<img src="<?php echo $this->_asset('default/images/eventos/index/7.jpg'); ?>" alt="">
							<span class="title">I Mostra Noivas Aracaju</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->_url('eventos/index/k/6/g/4-bazar-noivas'); ?>" data-modal>
							<img src="<?php echo $this->_asset('default/images/eventos/index/6.jpg'); ?>" alt="">
							<span class="title">4º Bazar de Noivas do Ateliê Margareth Souvenirs</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->_url('eventos/index/k/5/g/revista-casar-se-2013'); ?>" data-modal>
							<img src="<?php echo $this->_asset('default/images/eventos/index/5.jpg'); ?>" alt="">
							<span class="title">Lançamento da revista Casar-SE 2013</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->_url('eventos/index/k/4/g/expo-noivas-itabaiana-2012'); ?>" data-modal>
							<img src="<?php echo $this->_asset('default/images/eventos/index/4.jpg'); ?>" alt="">
							<span class="title">1º Expo Noivas de Itabaiana-SE</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->_url('eventos/index/k/3/g/workshop-casar-se-2012'); ?>" data-modal>
							<img src="<?php echo $this->_asset('default/images/eventos/index/3.jpg'); ?>" alt="">
							<span class="title">Workshop Casar-SE 2012</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->_url('eventos/index/k/2/g/revista-casar-se-2012'); ?>" data-modal>
							<img src="<?php echo $this->_asset('default/images/eventos/index/2.jpg'); ?>" alt="">
							<span class="title">Lançamento da revista Casar-SE 2012</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $this->_url('eventos/index/k/1/g/noiva-fest-2011'); ?>" data-modal>
							<img src="<?php echo $this->_asset('default/images/eventos/index/1.jpg'); ?>" alt="">
							<span class="title">Noiva Fest 2011</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- Hiperbanner -->
<div class="section-page section-page-b"></div>

<!-- Orçamento -->
<div class="section-page" id="orcamento">
	<div class="section-page-container">
		<header class="section-page-header">
			<h1>Orçamento</h1>
			<h3>Solicite já o seu orçamento</h3>
		</header>

		<div class="section-page-content">
			<div class="block-pane">
				<form id="form1" method="post" action="<?php echo $this->_url('form/orcamento'); ?>" data-form-ajax class="form">
					<fieldset>
						<legend>Orçamento</legend>

						<div class="grid grid-items-2">
							<div class="grid-item">
								<label>Nome *<input name="name" type="text" required></label>
								<label>Email *<input name="email" type="email" required></label>
								<label>Telefone *<input name="phone" type="text" data-field-mask="phone" required></label>
							</div>
							<div class="grid-item">
								<label>Produto *
									<select name="product" required>
										<option value="" disabled selected>Selecione</option>
										<option value="Nascimento">Nascimento</option>
										<option value="Batizado">Batizado</option>
										<option value="Eucaristia">Eucaristia</option>
										<option value="15 anos">15 anos</option>
										<option value="Casamento">Casamento</option>
										<option value="Formatura">Formatura</option>
										<option value="Corporativo">Corporativo</option>
										<option value="Bodas">Bodas</option>
										<option value="Outros">Outros</option>
									</select>
								</label>
								<label>Detalhes do pedido *<textarea name="details" cols="30" rows="10" maxlength="600" data-field-count class="height-115" required></textarea></label>
							</div>
						</div>

						<div class="block-action">
							<button type="submit" class="button button-large button-2">Enviar</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Contato -->
<div class="section-page section-page-c" id="contato">
	<div class="section-page-container">
		<header class="section-page-header">
			<h1>Contato</h1>
			<h3>Para mais informações, entre em contato conosco</h3>
		</header>

		<div class="section-page-content">
			<div class="grid grid-items-2">
				<div class="grid-item">
					<div class="block-pane-2">
						<address>
							<strong>Ateliê Margareth Souvenirs</strong><br>
							Rua Aloísio Braga, 387<br>
							Bairro Suíssa<br>
							Aracaju-SE, CEP 49050-050
						</address>
						<div class="phone">
							<a href="tel:07930417730">(79) 3041-7730</a> (Fixo)<br>
							<a href="tel:07999890156">(79) 9989-0156</a> (Celular)
						</div>
						<div class="email"><a href="mailto:margarethfesta@bol.com.br">margarethfesta@bol.com.br</a></div>
						<div class="map separate">
							<div class="block-embed">
								<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15669.880662151561!2d-37.06354!3d-10.927813!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x43027af1e2aa1b1!2sAteli%C3%AA+Margareth+Souvenirs!5e0!3m2!1sen!2sbr!4v1429291892229" width="425" height="350" frameborder="0" style="border:0"></iframe>
							</div>
						</div>
					</div>
				</div>
				<div class="grid-item">
					<div class="block-pane">
						<form id="form2" method="post" action="<?php echo $this->_url('form/contato'); ?>" data-form-ajax class="form">
							<fieldset>
								<legend>Contato</legend>
								<label>Nome *<input name="name" type="text" required></label>
								<label>Email *<input name="email" type="email" required></label>
								<label>Telefone *<input name="phone" type="text" data-field-mask="phone" required></label>
								<label>Mensagem *<textarea name="message" cols="30" rows="10" maxlength="600" data-field-count class="height-100" required></textarea></label>
								<div class="block-action">
									<button type="submit" class="button button-large button-2">Enviar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
