<div class="heading-page">
    <h2>Help</h2>
</div>

<div class="block-group block-box">
    <form action="" class="form form-inline">
        <fieldset>
            <legend>Filtro</legend>

            <div class="grid grid-items-5">
                <div class="grid-item">
                    <label><input name="q" type="text" placeholder="Pesquisar"></label>
                </div>
                <div class="grid-item">
                    <button type="submit" class="button button-info"><span class="icon-search"></span></button>
                </div>
            </div>

            <!-- <div class="separate">
                <ul class="list-link-inline">
                    <li>Resultado da pesquisa: <strong>4 itens encontrados</strong></li>
                    <li><a href="#">Limpar pesquisa</a></li>
                </ul>
            </div> -->
        </fieldset>
    </form>
</div>

<div class="block-group">
    <div class="collapse">
        <h4 class="collapse-heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h4>
        <div class="collapse-container">
            <div class="text">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis sit illum architecto expedita maiores dolorum assumenda culpa perspiciatis error ipsa vero ratione, reprehenderit, accusamus laboriosam voluptates cum tenetur delectus corrupti.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora, ipsa dolorum soluta eligendi, vitae culpa enim rem odio ab molestias eaque similique quasi! In ab reiciendis iure tenetur, facilis suscipit.</p>
            </div>
        </div>
    </div>

    <div class="collapse">
        <h4 class="collapse-heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h4>
        <div class="collapse-container">
            <div class="text">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis sit illum architecto expedita maiores dolorum assumenda culpa perspiciatis error ipsa vero ratione, reprehenderit, accusamus laboriosam voluptates cum tenetur delectus corrupti.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora, ipsa dolorum soluta eligendi, vitae culpa enim rem odio ab molestias eaque similique quasi! In ab reiciendis iure tenetur, facilis suscipit.</p>
            </div>
        </div>
    </div>

    <div class="collapse">
        <h4 class="collapse-heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit</h4>
        <div class="collapse-container">
            <div class="text">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis sit illum architecto expedita maiores dolorum assumenda culpa perspiciatis error ipsa vero ratione, reprehenderit, accusamus laboriosam voluptates cum tenetur delectus corrupti.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora, ipsa dolorum soluta eligendi, vitae culpa enim rem odio ab molestias eaque similique quasi! In ab reiciendis iure tenetur, facilis suscipit.</p>
            </div>
        </div>
    </div>
</div>

