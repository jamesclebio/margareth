<div class="heading-page">
    <h2>Table</h2>

    <ul class="action">
        <li><a href="#" class="button button-success"><span class="icon-plus-circled"></span> Novo</a></li>
    </ul>
</div>

<div class="block-group block-box">
    <form action="" class="form form-inline">
        <fieldset>
            <legend>Filtro</legend>

            <div class="grid grid-items-5">
                <div class="grid-item">
                    <label><input name="q" type="text" placeholder="Pesquisar"></label>
                </div>
                <div class="grid-item">
                    <label>
                        <select data-name="test" class="active">
                            <option value="" disabled>Campo</option>
                            <option value="">-</option>
                            <option data-name="test_field_1" value="1">Option 1</option>
                            <option data-name="test_field_2" value="2" selected>Option 2</option>
                            <option data-name="test_field_3" value="3">Option 3</option>
                        </select>
                    </label>
                </div>
                <div class="grid-item">
                    <label>
                        <select data-name="test">
                            <option value="" disabled selected>Campo</option>
                            <option value="">-</option>
                            <option data-name="test_field_1" value="1">Option 1</option>
                            <option data-name="test_field_2" value="2">Option 2</option>
                            <option data-name="test_field_3" value="3">Option 3</option>
                        </select>
                    </label>
                </div>
                <div class="grid-item">
                    <button type="submit" class="button button-info"><span class="icon-search"></span></button>
                </div>
            </div>

            <div class="separate">
                <ul class="list-link-inline">
                    <li>Resultado da pesquisa: <strong>4 itens encontrados</strong></li>
                    <li><a href="#">Limpar pesquisa</a></li>
                </ul>
            </div>
        </fieldset>
    </form>
</div>

<form action="" class="form">
    <div class="block-group">
        <table class="table">
            <thead>
                <tr>
                    <th><input id="action-toggle" type="checkbox"></th>
                    <th class="order-asc">
                        <a href="#">Label</a>
                        <a href="#" class="order" title="Remover ordenação">1</a>
                    </th>
                    <th><a href="#">Label</a></th>
                    <th><a href="#">Total (R$)</a></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox" name="" value="" class="action-select"></td>
                    <td class="field-code"><a href="#">Lorem ipsum dolor</a></td>
                    <td>Lorem ipsum dolor</td>
                    <td class="field-price">9.999,99</td>
                    <td class="field-action">
                        <a href="#" class="button-short button-warning" title="Mais"><span class="icon-more"></span></a>
                        <a href="#" class="button-short button-success" title="Editar"><span class="icon-edit"></span></a>
                        <a href="#" class="button-short button-danger" title="Excluir"><span class="icon-trash-a"></span></a>
                    </td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="" value="" class="action-select"></td>
                    <td class="field-code"><a href="#">Lorem ipsum dolor</a></td>
                    <td>Lorem ipsum dolor</td>
                    <td class="field-price">9.999,99</td>
                    <td class="field-action">
                        <a href="#" class="button-short button-warning" title="Mais"><span class="icon-more"></span></a>
                        <a href="#" class="button-short button-success" title="Editar"><span class="icon-edit"></span></a>
                        <a href="#" class="button-short button-danger" title="Excluir"><span class="icon-trash-a"></span></a>
                    </td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="" value="" class="action-select"></td>
                    <td class="field-code"><a href="#">Lorem ipsum dolor</a></td>
                    <td>Lorem ipsum dolor</td>
                    <td class="field-price">9.999,99</td>
                    <td class="field-action">
                        <a href="#" class="button-short button-warning" title="Mais"><span class="icon-more"></span></a>
                        <a href="#" class="button-short button-success" title="Editar"><span class="icon-edit"></span></a>
                        <a href="#" class="button-short button-danger" title="Excluir"><span class="icon-trash-a"></span></a>
                    </td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="" value="" class="action-select"></td>
                    <td class="field-code"><a href="#">Lorem ipsum dolor</a></td>
                    <td>Lorem ipsum dolor</td>
                    <td class="field-price">9.999,99</td>
                    <td class="field-action">
                        <a href="#" class="button-short button-warning" title="Mais"><span class="icon-more"></span></a>
                        <a href="#" class="button-short button-success" title="Editar"><span class="icon-edit"></span></a>
                        <a href="#" class="button-short button-danger" title="Excluir"><span class="icon-trash-a"></span></a>
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- <div class="alert-short alert-short-empty">
            <h3>Ops!</h3>
            <p>Nenhum conteúdo para exibir aqui.</p>
        </div> -->
    </div>

    <div class="block-group separate">
        <ul class="list-link-inline margin-bottom-15">
            <li><span class="icon-ios-arrow-thin-up"></span> Selecionado <strong><span class="field-check-checked"></span></strong> de <strong><span class="field-check-total"></span></strong></li>
            <li><a href="#" class="field-check-all">Todos</a></li>
            <li><a href="#" class="field-check-none">Nenhum</a></li>
        </ul>

        <div class="grid grid-items-4 form-inline">
            <div class="grid-item">
                <label>
                    <select name="" >
                        <option value="" disabled selected>Ação</option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                    </select>
                </label>
            </div>
            <div class="grid-item">
                <button type="submit" class="button">Aplicar</button>
            </div>
        </div>
    </div>

    <div class="separate">
        <div class="block-group">
            <div class="pagination">
                <ul>
                    <li><a href="#"><span class="icon-arrow-left-b"></span></a></li>
                    <li class="is-current active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><span>...</span></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li><a href="#">8</a></li>
                    <li><a href="#">9</a></li>
                    <li><a href="#">10</a></li>
                    <li><a href="#"><span class="icon-arrow-right-b"></span></a></li>
                </ul>
            </div>
        </div>

        <ul class="list-link-inline align-center">
            <li><strong>1 - 4</strong> de <strong>10</strong> itens</li>
            <li><a href="#">Mostrar todos</a></li>
        </ul>
    </div>
</form>

