<?php
class GalleryHelper
{
	private $name = 'gallery';
	private $path;
	private $fileTypes = array('jpg', 'jpeg', 'png');

	public function setName($name) {
		$this->name = $name;
	}

	public function setPath($path) {
		$this->path = $path;
	}

	public function builder() {
		$directory = @opendir($this->path);

		if ($directory) {
			while ($file = readdir($directory)) {
				if (in_array(substr(strtolower($file), strrpos($file, '.') + 1), $this->fileTypes)) {
					echo '<li><a href="' . $this->path . $file . '" rel="' . $this->name . '" class="lightbox"><img src="' . $this->path . $file . '" alt=""></a></li>';
				}
			}

			closedir($directory);
		} else {
			echo '<div class="block-notice block-notice-empty"><p>Ops! Nenhuma imagem para mostrar aqui... :(</p></div>';
		}
	}
}
