<?php
class Form extends Page {
	public function index() {
		$this->setLayout(null);
		$this->setView(null);
	}

	public function orcamento() {
		if ($_POST) {
			$mail = new MailHelper();
			$mail->setHeader(
					'From: ' . $_POST['name'] . ' <' . $_POST['email'] . '>' . "\r\n" .
					'Reply-To: ' . $_POST['email'] . "\r\n"
			);
			// $mail->setTo('margarethfesta@bol.com.br');
			$mail->setTo('dev@jamesclebio.com.br');
			$mail->setSubject('[Margareth Souvenirs - Orçamento] ' . $_POST['name']);
			$mail->setBody(
					'Nome: ' . $_POST['name'] . "\n" .
					'E-mail: ' . $_POST['email'] . "\n" .
					'Telefone: ' . $_POST['phone'] . "\n\n" .
					'Produto: ' . $_POST['product'] . "\n\n" .
					'---' . "\n" .
					'Detalhes do pedido:' . "\n\n" .
					$_POST['details'] . "\n\n" .
					'*** Enviado a partir de ' . $_SERVER['HTTP_HOST'] . ' em ' . date('d/m/Y H:i:s')
			);
			$mail->send($this);
		}
	}

	public function contato() {
		if ($_POST) {
			$mail = new MailHelper();
			$mail->setHeader(
					'From: ' . $_POST['name'] . ' <' . $_POST['email'] . '>' . "\r\n" .
					'Reply-To: ' . $_POST['email'] . "\r\n"
			);
			// $mail->setTo('margarethfesta@bol.com.br');
			$mail->setTo('dev@jamesclebio.com.br');
			$mail->setSubject('[Margareth Souvenirs - Contato] ' . $_POST['name']);
			$mail->setBody(
					'Nome: ' . $_POST['name'] . "\n" .
					'E-mail: ' . $_POST['email'] . "\n" .
					'Telefone: ' . $_POST['phone'] . "\n\n" .
					'---' . "\n" .
					'Mensagem:' . "\n\n" .
					$_POST['message'] . "\n\n" .
					'*** Enviado a partir de ' . $_SERVER['HTTP_HOST'] . ' em ' . date('d/m/Y H:i:s')
			);
			$mail->send($this);
		}
	}
}
