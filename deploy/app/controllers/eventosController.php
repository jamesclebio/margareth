<?php
class Eventos extends Page
{
	public $gallery;

	public function index() {
		$this->setLayout(null);
		$this->setView('eventos');
		$this->setTitle('Ateliê Margareth Souvenirs');
		$this->setDescription('Ateliê com 30 anos de experiência, voltado para o mercado de lembranças para nascimento, batizado, eucaristia, debutantes, bodas, casamento e corporativos, utilizando as mais modernas e exclusivas linhas de produtos inovadores.');
		$this->setAnalytics(true);
	}

	public function galeria() {
		$this->setView('eventos/galeria');
		$this->gallery = new GalleryHelper();
		$this->gallery->setName($this->_get('g'));
		$this->gallery->setPath('assets/default/images/eventos/' . $this->_get('g') . '/');
		$this->gallery->builder();
	}
}
