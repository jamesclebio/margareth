<?php
class Sample extends Page
{
	public function index() {
		$this->setLayout('default');
		$this->setView('dashboard');
		$this->setTitle('Ateliê Margareth Souvenirs');
		$this->setDescription('Ateliê com 30 anos de experiência, voltado para o mercado de lembranças para nascimento, batizado, eucaristia, debutantes, bodas, casamento e corporativos, utilizando as mais modernas e exclusivas linhas de produtos inovadores.');
		$this->setAnalytics(true);
	}

	// Table
	public function table() {
		$this->setView('sample/table');
	}

	// Form
	public function form() {
		$this->setView('sample/form');
	}

	// Confirm
	public function confirm() {
		$this->setView('sample/confirm');
	}

	// Help
	public function help() {
		$this->setView('sample/help');
	}

	// Chart
	public function chart() {
		$this->setLayout(null);
		$this->setView(null);

		switch ($this->_get('type')) {
			case 'line':
				$this->setView('sample/chart-line');
				break;

			case 'column':
				$this->setView('sample/chart-column');
				break;

			case 'pie':
				$this->setView('sample/chart-pie');
				break;

			default:
				break;
		}
	}

	// Popup
	public function popup() {
		$this->setLayout('popup');
		$this->setView('sample/form');
	}
}

