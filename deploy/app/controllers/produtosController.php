<?php
class Produtos extends Page
{
	public $options = array();
	public $gallery;

	public function index() {
		$this->setLayout(null);
		$this->setView('produtos-opcoes');
		$this->setTitle('Ateliê Margareth Souvenirs');
		$this->setDescription('Ateliê com 30 anos de experiência, voltado para o mercado de lembranças para nascimento, batizado, eucaristia, debutantes, bodas, casamento e corporativos, utilizando as mais modernas e exclusivas linhas de produtos inovadores.');
		$this->setAnalytics(true);
	}

	public function galeria() {
		$this->setView('produtos-galeria');
		$this->gallery = new GalleryHelper();
		$this->gallery->setName($this->_get('t'));
		$this->gallery->setPath('assets/default/images/produtos/' . $this->_get('t') . '/' . $this->_get('s') . '/');
	}

	public function nascimento() {
		$this->options['diversos'] = 'Diversos';
	}

	public function batizado() {
		$this->options['diversos'] = 'Diversos';
	}

	public function eucaristia() {
		$this->options['diversos'] = 'Diversos';
	}

	public function quinzeanos() {
		$this->options['diversos'] = 'Diversos';
	}

	public function casamento() {
		$this->options['caixetas'] = 'Caixetas';
		$this->options['banheiro'] = 'Caixas para banheiro';
		$this->options['bemcasados'] = 'Embalagens para bem casados';
		$this->options['padrinhos'] = 'Lembranças para padrinhos';
		$this->options['livros'] = 'Livros';
		$this->options['guardanapos'] = 'Porta-guardanapos';
		$this->options['diversos'] = 'Diversos';
	}

	public function formatura() {
		$this->options['diversos'] = 'Diversos';
	}

	public function corporativo() {
		$this->options['diversos'] = 'Diversos';
	}

	public function bodas() {
		$this->options['diversos'] = 'Diversos';
	}

	public function outros() {
		$this->options['diversos'] = 'Diversos';
	}
}
