<?php
class Index extends Page
{
	public function index() {
		$this->setLayout('default');
		$this->setView('index');
		$this->setTitle('Ateliê Margareth Souvenirs');
		$this->setDescription('Ateliê com 30 anos de experiência, voltado para o mercado de lembranças para nascimento, batizado, eucaristia, debutantes, bodas, casamento e corporativos, utilizando as mais modernas e exclusivas linhas de produtos inovadores.');
		$this->setAnalytics(true);
	}
}

