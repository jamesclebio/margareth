<?php
/**
 * Basis
 *
 * @author James Clébio <jamesclebio@gmail.com>
 * @link https://github.com/jamesclebio/basis 
 * @license http://jamesclebio.mit-license.org/
 */

define('ASSETS', 'assets/');
define('CACHE', 'app/cache/');
define('CONTROLLERS', 'app/controllers/');
define('CORE', 'app/basis/');
define('HELPERS', 'app/helpers/');
define('INCLUDES', 'app/views/includes/');
define('LAYOUTS', 'app/layouts/');
define('MODELS', 'app/models/');
define('TIMEZONE', 'America/Sao_Paulo');
define('VIEWS', 'app/views/');

$controller_routes = array(
	'home' => 'index'
);

