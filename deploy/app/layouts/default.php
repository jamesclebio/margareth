<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> no-js" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $this->getTitle(); ?></title>
	<meta name="description" content="<?php echo $this->getDescription(); ?>">
	<meta name="robots" content="noindex">
	<link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
	<script src="<?php echo $this->_asset('default/scripts/head.js'); ?>"></script>
	<?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<header class="default-header">
		<div class="default-header-container">
			<h1 class="main-logo"><a href="<?php echo $this->_url('root'); ?>">Ateliê Margareth Souvenirs</a></h1>
			<nav class="main-nav">
				<ul>
					<li><a href="#!/inicio">Início</a></li>
					<li><a href="#!/atelie">O ateliê</a></li>
					<li><a href="#!/produtos">Nossos produtos</a></li>
					<li><a href="#!/eventos">Eventos</a></li>
					<li><a href="#!/orcamento">Orçamento</a></li>
					<li><a href="#!/contato">Contato</a></li>
				</ul>
			</nav>
		</div>
	</header>

	<?php $this->getView(); ?>

	<div class="default-social">
		<h3>Acompanhe-nos nas redes sociais</h3>
		<ul>
			<li class="facebook"><a href="https://www.facebook.com/margareth.souvenirs" target="_blank">Facebook</a></li>
			<li class="instagram"><a href="https://instagram.com/margarethsouvenirs/" target="_blank">Instagram</a></li>
		</ul>
	</div>

	<footer class="default-footer">
		<div class="logo-margareth">
			<img src="<?php echo $this->_asset('default/images/logo-margareth.png'); ?>" alt="Logo Ateliê Margareth Souvenirs">
		</div>

		<div class="contact">
			<div class="phone"><a href="tel:07930417730">(79) 3041-7730</a> / <a href="tel:07999890156">(79) 9989-0156</a></div>
			<div class="email"><a href="mailto:margarethfesta@bol.com.br">margarethfesta@bol.com.br</a></div>
		</div>

		<div class="share">
			<h6 class="heading">Compartilhe</h6>
			<?php include 'share.php' ?>
		</div>
	</footer>

	<button type="button" data-toggle-roll="40|display-block" data-roll-top title="Ir para o topo"></button>

	<script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>
