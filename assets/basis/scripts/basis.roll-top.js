;(function($, window, document, undefined) {
	'use strict';

	basis.rollTop = {
		settings: {
			autoinit: true,
			main: '[data-roll-top]'
		},

		init: function() {
			if ($(this.settings.main).length) {
				this.handler();
			}
		},

		handler: function() {
			var	that = this;

			$(document).on({
				click: function(e) {
					basis.roll.to();
					e.preventDefault();
				}
			}, that.settings.main);
		}
	};
}(jQuery, this, this.document));

