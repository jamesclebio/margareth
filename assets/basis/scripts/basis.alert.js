;(function($, window, document, undefined) {
	'use strict';

	basis.alert = {
		settings: {
			autoinit: true,
			main: '.alert',
			template_dismiss: '<button type="button" class="alert-dismiss" data-alert-dismiss></button>'
		},

		init: function() {
			this.handler();

			if ($(this.settings.main).length) {
				this.builder();
			}
		},

		handler: function() {
			var	that = this;

			$(document).on({
				click: function(e) {
					$(this).closest(that.settings.main).fadeOut(100, function() {
						$(this).remove();
					});

					e.preventDefault();
				}
			}, '[data-alert-dismiss]');
		},

		builder: function() {
			var	that = this,
				$main = $(that.settings.main);

			$main.each(function() {
				if ($(this).data('alertClose')) {
					$(this).prepend(that.settings.template_dismiss);
				}
			});
		}
	};
}(jQuery, this, this.document));

