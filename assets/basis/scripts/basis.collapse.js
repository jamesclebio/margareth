;(function($, window, document, undefined) {
	'use strict';

	basis.collapse = {
		settings: {
			autoinit: true,
			main: '.collapse'
		},

		init: function() {
			if ($(this.settings.main).length) {
				this.handler();
			}
		},

		handler: function() {
			var	that = this,
				$main = $(that.settings.main);

			$main.find('.collapse-heading').on({
				click: function() {
					that.toggle($(this));
				}
			});
		},

		toggle: function($this) {
			var	$collapse = $this.closest(this.settings.main),
				class_open = 'is-open';

			$collapse.find('.collapse-container').fadeToggle(100, function() {
				if ($(this).is(':visible')) {
					$collapse.addClass(class_open);
				} else {
					$collapse.removeClass(class_open);
				}
			});
		}
	};
}(jQuery, this, this.document));

